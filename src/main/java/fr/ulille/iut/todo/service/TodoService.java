package fr.ulille.iut.todo.service;

import java.util.List;
import java.util.UUID;

import javax.swing.plaf.basic.BasicTreeUI.TreeHomeAction;

import org.jvnet.hk2.internal.ErrorResults;

import fr.ulille.iut.todo.BDDFactory;
import fr.ulille.iut.todo.dao.TacheDAO;
import jakarta.ws.rs.WebApplicationException;

public class TodoService {
    private TacheDAO taches;

    public TodoService() {
        taches = BDDFactory.buildDao(TacheDAO.class);
        taches.createTable();
    }

    public Tache getTache(UUID id) {
    	return taches.getById(id.toString());
    }
    public Tache getTache(String description) {
    	return taches.getByDescription(description);
    }
    public List<Tache> getAll() {
        return taches.getAll();
    }

    public void addTache(Tache newTache) {
        taches.insert(newTache);
    }

    public int deleteTache(String id) {
        return taches.DeleteById(id) ;
    }

    public void updateTache(Tache tache) {
        return;
    }
}
