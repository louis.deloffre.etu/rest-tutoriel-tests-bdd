package fr.ulille.iut.todo.dao;

import java.util.List;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.todo.service.Tache;

/**
 * TacheDAO
 */
public interface TacheDAO {
	
	@SqlQuery("select * from taches where id=?")
	@RegisterBeanMapper(Tache.class)
	Tache getById( String id);
	
	@SqlQuery("select * from taches where description=?")
	@RegisterBeanMapper(Tache.class)
	Tache getByDescription( String description);
	
	@SqlQuery("delete from taches where id=?")
	@RegisterBeanMapper(Tache.class)
	int DeleteById( String id);
	
    @SqlUpdate("create table if not exists taches (id varchar(128) primary key, nom varchar not null, description varchar)")
    void createTable();

    @SqlUpdate("drop table if exists taches")
    void dropTable();

    @SqlUpdate("insert into taches (id, nom, description) values (:id, :nom, :description)")
    int insert(@BindBean Tache tache);

    @SqlQuery("select * from taches")
    @RegisterBeanMapper(Tache.class)
    List<Tache> getAll();
    
    
}

